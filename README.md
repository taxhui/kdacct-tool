# KDAcctTool

#### 介绍
金蝶账套批量附加/分离;

账套密码清除;

配置表清除;

账套参数设置;

执行SQL语句;

常用SQL语句收藏

upd.txt 更新服务器地址

KDAcctTool.xml  更新配置文件

#### 第三方库引用
MiniExcel  [https://gitee.com/dotnetchina/MiniExcel](https://gitee.com/dotnetchina/MiniExcel)

AutoUpdater.NET [https://github.com/ravibpatel/AutoUpdater.NET](https://github.com/ravibpatel/AutoUpdater.NET)

SQLSugar [https://gitee.com/dotnetchina/SqlSugar](https://gitee.com/dotnetchina/SqlSugar)

Costura.Fody [https://github.com/Fody/Costura](https://github.com/Fody/Costura)

Furion  [https://gitee.com/dotnetchina/Furion](https://gitee.com/dotnetchina/Furion)


