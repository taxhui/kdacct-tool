﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KDAcctAttach.Models {
    public class KDConfigModel {
        public string FKey { get; set; }
        public string FValue { get; set; }
        public string FCategory { get; set; }
    }
}
