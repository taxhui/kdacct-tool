﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KDAcctAttach {
    public class AcctItemModel {
        public string AcctName { get; set; }
        public string MDFName { get; set; }
        public string LDFName { get; set; }
        public string ErrInfo { get; set; }
    }
}
