﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KDAcctAttach {
    public class DBItemModel {
        public bool Checked { get; set; }
        public string DBName { get; set; }
        public string DBId { get; set; }
        public string DBPath { get; set; }
        public string DBVersion { get; set; }
        public string CMPTLevel { get; set; }
        public List<DBFilesModel> Files { get; set; }
    }
    public class DBFilesModel
    {
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileSize { get; set; }
    }
}
