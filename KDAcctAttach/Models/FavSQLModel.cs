﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace KDAcctAttach.Models {
    
    public class FavSQLModel {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime LastUpdate { get; set; }
        public string QueryString { get; set; }
        public string QueryName { get; set; }
        public string Description { get; set; }
        public E_YesNo Shared { get; set; }
    }
}
