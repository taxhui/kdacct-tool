﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KDAcctAttach {
    public enum E_ServerVersion {
        MSSQL7 = 515,
        MSSQL2000 = 539,
        MSSQL2005 = 611,
        MSSQL2005vardecimal = 612,
        MSSQL2008 = 661,
        MSSQL2008R2 = 665,
        MSSQL2012 = 706,
        MSSQL2014 = 782,
        MSSQL2016= 852,
        MSSQL2017 = 869,
        MSSQL2019=904
    }
}
