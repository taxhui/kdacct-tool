﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KDAcctAttach {
    public class DBConfigModel {
        public string Server { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DBName { get; set; }
        public bool LocalDB { get; set; }
    }
}
