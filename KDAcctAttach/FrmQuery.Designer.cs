﻿namespace KDAcctAttach {
    partial class FrmQuery {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.plLeft = new System.Windows.Forms.Panel();
            this.dgvDBList = new System.Windows.Forms.DataGridView();
            this.colDBName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dBIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dBPathDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dBVersionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cMPTLevelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsDBList = new System.Windows.Forms.BindingSource(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnGetDBList = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgvQueryResult = new System.Windows.Forms.DataGridView();
            this.plRight = new System.Windows.Forms.Panel();
            this.tbxQuery = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tbxResult = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnExportResult = new System.Windows.Forms.Button();
            this.btnCommand = new System.Windows.Forms.Button();
            this.btnQuery = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.plLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDBList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDBList)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQueryResult)).BeginInit();
            this.plRight.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(933, 12);
            this.panel1.TabIndex = 0;
            this.panel1.Visible = false;
            // 
            // plLeft
            // 
            this.plLeft.Controls.Add(this.dataGridView1);
            this.plLeft.Controls.Add(this.dgvDBList);
            this.plLeft.Controls.Add(this.panel5);
            this.plLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.plLeft.Location = new System.Drawing.Point(0, 12);
            this.plLeft.Name = "plLeft";
            this.plLeft.Size = new System.Drawing.Size(349, 668);
            this.plLeft.TabIndex = 1;
            // 
            // dgvDBList
            // 
            this.dgvDBList.AllowUserToAddRows = false;
            this.dgvDBList.AllowUserToDeleteRows = false;
            this.dgvDBList.AllowUserToResizeRows = false;
            this.dgvDBList.AutoGenerateColumns = false;
            this.dgvDBList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDBList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDBName,
            this.dBIdDataGridViewTextBoxColumn,
            this.dBPathDataGridViewTextBoxColumn,
            this.dBVersionDataGridViewTextBoxColumn,
            this.cMPTLevelDataGridViewTextBoxColumn});
            this.dgvDBList.DataSource = this.bsDBList;
            this.dgvDBList.Location = new System.Drawing.Point(0, 34);
            this.dgvDBList.Name = "dgvDBList";
            this.dgvDBList.ReadOnly = true;
            this.dgvDBList.RowHeadersVisible = false;
            this.dgvDBList.RowTemplate.Height = 23;
            this.dgvDBList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDBList.Size = new System.Drawing.Size(349, 154);
            this.dgvDBList.TabIndex = 4;
            // 
            // colDBName
            // 
            this.colDBName.DataPropertyName = "DBName";
            this.colDBName.HeaderText = "数据库名";
            this.colDBName.Name = "colDBName";
            this.colDBName.ReadOnly = true;
            this.colDBName.Width = 150;
            // 
            // dBIdDataGridViewTextBoxColumn
            // 
            this.dBIdDataGridViewTextBoxColumn.DataPropertyName = "DBId";
            this.dBIdDataGridViewTextBoxColumn.HeaderText = "DBId";
            this.dBIdDataGridViewTextBoxColumn.Name = "dBIdDataGridViewTextBoxColumn";
            this.dBIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dBPathDataGridViewTextBoxColumn
            // 
            this.dBPathDataGridViewTextBoxColumn.DataPropertyName = "DBPath";
            this.dBPathDataGridViewTextBoxColumn.HeaderText = "主路径";
            this.dBPathDataGridViewTextBoxColumn.Name = "dBPathDataGridViewTextBoxColumn";
            this.dBPathDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dBVersionDataGridViewTextBoxColumn
            // 
            this.dBVersionDataGridViewTextBoxColumn.DataPropertyName = "DBVersion";
            this.dBVersionDataGridViewTextBoxColumn.HeaderText = "版本";
            this.dBVersionDataGridViewTextBoxColumn.Name = "dBVersionDataGridViewTextBoxColumn";
            this.dBVersionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cMPTLevelDataGridViewTextBoxColumn
            // 
            this.cMPTLevelDataGridViewTextBoxColumn.DataPropertyName = "CMPTLevel";
            this.cMPTLevelDataGridViewTextBoxColumn.HeaderText = "兼容级别";
            this.cMPTLevelDataGridViewTextBoxColumn.Name = "cMPTLevelDataGridViewTextBoxColumn";
            this.cMPTLevelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bsDBList
            // 
            this.bsDBList.DataSource = typeof(KDAcctAttach.DBItemModel);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnGetDBList);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(349, 34);
            this.panel5.TabIndex = 0;
            // 
            // btnGetDBList
            // 
            this.btnGetDBList.Location = new System.Drawing.Point(12, 6);
            this.btnGetDBList.Name = "btnGetDBList";
            this.btnGetDBList.Size = new System.Drawing.Size(112, 23);
            this.btnGetDBList.TabIndex = 0;
            this.btnGetDBList.Text = "刷新数据库列表";
            this.btnGetDBList.UseVisualStyleBackColor = true;
            this.btnGetDBList.Click += new System.EventHandler(this.BtnGetDBList_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvQueryResult);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(349, 391);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(584, 289);
            this.panel3.TabIndex = 2;
            // 
            // dgvQueryResult
            // 
            this.dgvQueryResult.AllowUserToAddRows = false;
            this.dgvQueryResult.AllowUserToDeleteRows = false;
            this.dgvQueryResult.AllowUserToResizeRows = false;
            this.dgvQueryResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvQueryResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvQueryResult.Location = new System.Drawing.Point(0, 0);
            this.dgvQueryResult.Name = "dgvQueryResult";
            this.dgvQueryResult.ReadOnly = true;
            this.dgvQueryResult.RowHeadersVisible = false;
            this.dgvQueryResult.RowTemplate.Height = 23;
            this.dgvQueryResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvQueryResult.Size = new System.Drawing.Size(584, 289);
            this.dgvQueryResult.TabIndex = 5;
            this.dgvQueryResult.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvQueryResult_DataError);
            // 
            // plRight
            // 
            this.plRight.Controls.Add(this.tbxQuery);
            this.plRight.Controls.Add(this.panel7);
            this.plRight.Controls.Add(this.panel6);
            this.plRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plRight.Location = new System.Drawing.Point(349, 12);
            this.plRight.Name = "plRight";
            this.plRight.Size = new System.Drawing.Size(584, 379);
            this.plRight.TabIndex = 3;
            // 
            // tbxQuery
            // 
            this.tbxQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxQuery.Font = new System.Drawing.Font("Microsoft YaHei", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbxQuery.Location = new System.Drawing.Point(0, 34);
            this.tbxQuery.Multiline = true;
            this.tbxQuery.Name = "tbxQuery";
            this.tbxQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxQuery.Size = new System.Drawing.Size(584, 216);
            this.tbxQuery.TabIndex = 3;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.tbxResult);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 250);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(584, 129);
            this.panel7.TabIndex = 2;
            // 
            // tbxResult
            // 
            this.tbxResult.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbxResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxResult.Font = new System.Drawing.Font("Microsoft YaHei", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbxResult.Location = new System.Drawing.Point(0, 0);
            this.tbxResult.Multiline = true;
            this.tbxResult.Name = "tbxResult";
            this.tbxResult.ReadOnly = true;
            this.tbxResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxResult.Size = new System.Drawing.Size(584, 129);
            this.tbxResult.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.button2);
            this.panel6.Controls.Add(this.button1);
            this.panel6.Controls.Add(this.btnExportResult);
            this.panel6.Controls.Add(this.btnCommand);
            this.panel6.Controls.Add(this.btnQuery);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(584, 34);
            this.panel6.TabIndex = 1;
            // 
            // btnExportResult
            // 
            this.btnExportResult.Location = new System.Drawing.Point(172, 6);
            this.btnExportResult.Name = "btnExportResult";
            this.btnExportResult.Size = new System.Drawing.Size(74, 23);
            this.btnExportResult.TabIndex = 2;
            this.btnExportResult.Text = "导出结果";
            this.btnExportResult.UseVisualStyleBackColor = true;
            this.btnExportResult.Click += new System.EventHandler(this.BtnExportResult_Click);
            // 
            // btnCommand
            // 
            this.btnCommand.Location = new System.Drawing.Point(92, 6);
            this.btnCommand.Name = "btnCommand";
            this.btnCommand.Size = new System.Drawing.Size(74, 23);
            this.btnCommand.TabIndex = 1;
            this.btnCommand.Text = "执行命令";
            this.btnCommand.UseVisualStyleBackColor = true;
            this.btnCommand.Click += new System.EventHandler(this.BtnCommand_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(12, 6);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(74, 23);
            this.btnQuery.TabIndex = 0;
            this.btnQuery.Text = "执行查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.BtnQuery_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(255, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(74, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "保存语句";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(335, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(74, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "保存语句";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dataGridView1.DataSource = this.bsDBList;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView1.Location = new System.Drawing.Point(0, 382);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(349, 286);
            this.dataGridView1.TabIndex = 5;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "DBName";
            this.dataGridViewTextBoxColumn1.HeaderText = "数据库名";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DBId";
            this.dataGridViewTextBoxColumn2.HeaderText = "DBId";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "DBPath";
            this.dataGridViewTextBoxColumn3.HeaderText = "主路径";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "DBVersion";
            this.dataGridViewTextBoxColumn4.HeaderText = "版本";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "CMPTLevel";
            this.dataGridViewTextBoxColumn5.HeaderText = "兼容级别";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // FrmQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 680);
            this.Controls.Add(this.plRight);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.plLeft);
            this.Controls.Add(this.panel1);
            this.Name = "FrmQuery";
            this.Text = "FrmQuery";
            this.Shown += new System.EventHandler(this.FrmQuery_Shown);
            this.plLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDBList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDBList)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvQueryResult)).EndInit();
            this.plRight.ResumeLayout(false);
            this.plRight.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel plLeft;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel plRight;
        private System.Windows.Forms.Button btnGetDBList;
        private System.Windows.Forms.DataGridView dgvDBList;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnCommand;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDBName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dBIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dBPathDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dBVersionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cMPTLevelDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource bsDBList;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox tbxResult;
        private System.Windows.Forms.DataGridView dgvQueryResult;
        private System.Windows.Forms.TextBox tbxQuery;
        private System.Windows.Forms.Button btnExportResult;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
    }
}