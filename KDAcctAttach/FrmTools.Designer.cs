﻿namespace KDAcctAttach {
    partial class FrmTools {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.plLeft = new System.Windows.Forms.Panel();
            this.dgvAcctList = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnRefreshDB = new System.Windows.Forms.Button();
            this.rgAllDB = new System.Windows.Forms.RadioButton();
            this.rgKDAcct = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSaveAcctConfig = new System.Windows.Forms.Button();
            this.btnGetAcctConfig = new System.Windows.Forms.Button();
            this.grpUsers = new System.Windows.Forms.GroupBox();
            this.dgvUserList = new System.Windows.Forms.DataGridView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnCleanProfile = new System.Windows.Forms.Button();
            this.btnCleanPass = new System.Windows.Forms.Button();
            this.btnGetUserList = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.rgCalculateType1 = new System.Windows.Forms.RadioButton();
            this.rgCalculateType0 = new System.Windows.Forms.RadioButton();
            this.chkBatchManual = new System.Windows.Forms.CheckBox();
            this.plRight = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chkJOBPAY_NOTCONTROLMAXROW = new System.Windows.Forms.CheckBox();
            this.chkICMORptAutoBuildItemScrap = new System.Windows.Forms.CheckBox();
            this.chkICMOCONFIRM_RELEASE = new System.Windows.Forms.CheckBox();
            this.chkUnderStockCalculate = new System.Windows.Forms.CheckBox();
            this.chkUnderStock = new System.Windows.Forms.CheckBox();
            this.chkSameUserApprovalAndUnApproval = new System.Windows.Forms.CheckBox();
            this.chkRefreshAfterAdd = new System.Windows.Forms.CheckBox();
            this.chkPOrderTaxInPrice = new System.Windows.Forms.CheckBox();
            this.chkSEOrderCanAppenItem = new System.Windows.Forms.CheckBox();
            this.chkPOOrderCanAppenItem = new System.Windows.Forms.CheckBox();
            this.chkPoInvSynInPrice = new System.Windows.Forms.CheckBox();
            this.chkNoHookSIWhenDiffQty = new System.Windows.Forms.CheckBox();
            this.chkNoHookPIWhenDiffQty = new System.Windows.Forms.CheckBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.rgItemCountPrice1 = new System.Windows.Forms.RadioButton();
            this.rgItemCountPrice0 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.rgInInvProcess1 = new System.Windows.Forms.RadioButton();
            this.rgInInvProcess0 = new System.Windows.Forms.RadioButton();
            this.chkCQtyLargeSEQty = new System.Windows.Forms.CheckBox();
            this.chkCQtyLargePOQty = new System.Windows.Forms.CheckBox();
            this.chkBatchRepeatEcho = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.fUserIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsUserList = new System.Windows.Forms.BindingSource(this.components);
            this.dBNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dBPathDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dBVersionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsDBList = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.rgUPSTOCKWHENSAVE1 = new System.Windows.Forms.RadioButton();
            this.rgUPSTOCKWHENSAVE0 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.plLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcctList)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.grpUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserList)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.plRight.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsUserList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDBList)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // plLeft
            // 
            this.plLeft.Controls.Add(this.dgvAcctList);
            this.plLeft.Controls.Add(this.panel4);
            this.plLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.plLeft.Location = new System.Drawing.Point(0, 0);
            this.plLeft.Name = "plLeft";
            this.plLeft.Size = new System.Drawing.Size(258, 604);
            this.plLeft.TabIndex = 0;
            // 
            // dgvAcctList
            // 
            this.dgvAcctList.AllowUserToAddRows = false;
            this.dgvAcctList.AllowUserToDeleteRows = false;
            this.dgvAcctList.AllowUserToResizeRows = false;
            this.dgvAcctList.AutoGenerateColumns = false;
            this.dgvAcctList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAcctList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dBNameDataGridViewTextBoxColumn,
            this.dBPathDataGridViewTextBoxColumn,
            this.dBVersionDataGridViewTextBoxColumn});
            this.dgvAcctList.DataSource = this.bsDBList;
            this.dgvAcctList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAcctList.Location = new System.Drawing.Point(0, 40);
            this.dgvAcctList.Name = "dgvAcctList";
            this.dgvAcctList.ReadOnly = true;
            this.dgvAcctList.RowHeadersVisible = false;
            this.dgvAcctList.RowTemplate.Height = 23;
            this.dgvAcctList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAcctList.Size = new System.Drawing.Size(258, 564);
            this.dgvAcctList.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnRefreshDB);
            this.panel4.Controls.Add(this.rgAllDB);
            this.panel4.Controls.Add(this.rgKDAcct);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(258, 40);
            this.panel4.TabIndex = 0;
            // 
            // btnRefreshDB
            // 
            this.btnRefreshDB.Location = new System.Drawing.Point(173, 9);
            this.btnRefreshDB.Name = "btnRefreshDB";
            this.btnRefreshDB.Size = new System.Drawing.Size(75, 23);
            this.btnRefreshDB.TabIndex = 2;
            this.btnRefreshDB.Text = "刷新列表";
            this.btnRefreshDB.UseVisualStyleBackColor = true;
            this.btnRefreshDB.Click += new System.EventHandler(this.BtnRefreshDB_Click);
            // 
            // rgAllDB
            // 
            this.rgAllDB.AutoSize = true;
            this.rgAllDB.Location = new System.Drawing.Point(84, 12);
            this.rgAllDB.Name = "rgAllDB";
            this.rgAllDB.Size = new System.Drawing.Size(83, 16);
            this.rgAllDB.TabIndex = 1;
            this.rgAllDB.Text = "所有数据库";
            this.rgAllDB.UseVisualStyleBackColor = true;
            // 
            // rgKDAcct
            // 
            this.rgKDAcct.AutoSize = true;
            this.rgKDAcct.Checked = true;
            this.rgKDAcct.Location = new System.Drawing.Point(7, 12);
            this.rgKDAcct.Name = "rgKDAcct";
            this.rgKDAcct.Size = new System.Drawing.Size(71, 16);
            this.rgKDAcct.TabIndex = 0;
            this.rgKDAcct.TabStop = true;
            this.rgKDAcct.Text = "金蝶账套";
            this.rgKDAcct.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSaveAcctConfig);
            this.panel2.Controls.Add(this.btnGetAcctConfig);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(258, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(541, 40);
            this.panel2.TabIndex = 1;
            // 
            // btnSaveAcctConfig
            // 
            this.btnSaveAcctConfig.Location = new System.Drawing.Point(87, 9);
            this.btnSaveAcctConfig.Name = "btnSaveAcctConfig";
            this.btnSaveAcctConfig.Size = new System.Drawing.Size(75, 23);
            this.btnSaveAcctConfig.TabIndex = 4;
            this.btnSaveAcctConfig.Text = "保存参数";
            this.btnSaveAcctConfig.UseVisualStyleBackColor = true;
            this.btnSaveAcctConfig.Click += new System.EventHandler(this.BtnSaveAcctConfig_Click);
            // 
            // btnGetAcctConfig
            // 
            this.btnGetAcctConfig.Location = new System.Drawing.Point(6, 10);
            this.btnGetAcctConfig.Name = "btnGetAcctConfig";
            this.btnGetAcctConfig.Size = new System.Drawing.Size(75, 23);
            this.btnGetAcctConfig.TabIndex = 3;
            this.btnGetAcctConfig.Text = "获取参数";
            this.btnGetAcctConfig.UseVisualStyleBackColor = true;
            this.btnGetAcctConfig.Click += new System.EventHandler(this.BtnGetAcctConfig_Click);
            // 
            // grpUsers
            // 
            this.grpUsers.Controls.Add(this.dgvUserList);
            this.grpUsers.Controls.Add(this.panel5);
            this.grpUsers.Dock = System.Windows.Forms.DockStyle.Left;
            this.grpUsers.Location = new System.Drawing.Point(0, 0);
            this.grpUsers.Name = "grpUsers";
            this.grpUsers.Size = new System.Drawing.Size(243, 564);
            this.grpUsers.TabIndex = 0;
            this.grpUsers.TabStop = false;
            this.grpUsers.Text = "用户列表";
            // 
            // dgvUserList
            // 
            this.dgvUserList.AllowUserToAddRows = false;
            this.dgvUserList.AllowUserToDeleteRows = false;
            this.dgvUserList.AllowUserToResizeRows = false;
            this.dgvUserList.AutoGenerateColumns = false;
            this.dgvUserList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUserList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fUserIDDataGridViewTextBoxColumn,
            this.fNameDataGridViewTextBoxColumn,
            this.fDescriptionDataGridViewTextBoxColumn});
            this.dgvUserList.DataSource = this.bsUserList;
            this.dgvUserList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvUserList.Location = new System.Drawing.Point(3, 49);
            this.dgvUserList.Name = "dgvUserList";
            this.dgvUserList.ReadOnly = true;
            this.dgvUserList.RowHeadersVisible = false;
            this.dgvUserList.RowTemplate.Height = 23;
            this.dgvUserList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUserList.Size = new System.Drawing.Size(237, 512);
            this.dgvUserList.TabIndex = 4;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnCleanProfile);
            this.panel5.Controls.Add(this.btnCleanPass);
            this.panel5.Controls.Add(this.btnGetUserList);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 17);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(237, 32);
            this.panel5.TabIndex = 5;
            // 
            // btnCleanProfile
            // 
            this.btnCleanProfile.Location = new System.Drawing.Point(147, 3);
            this.btnCleanProfile.Name = "btnCleanProfile";
            this.btnCleanProfile.Size = new System.Drawing.Size(57, 23);
            this.btnCleanProfile.TabIndex = 7;
            this.btnCleanProfile.Text = "清配置";
            this.btnCleanProfile.UseVisualStyleBackColor = true;
            this.btnCleanProfile.Click += new System.EventHandler(this.BtnCleanProfile_Click);
            // 
            // btnCleanPass
            // 
            this.btnCleanPass.Location = new System.Drawing.Point(84, 3);
            this.btnCleanPass.Name = "btnCleanPass";
            this.btnCleanPass.Size = new System.Drawing.Size(57, 23);
            this.btnCleanPass.TabIndex = 6;
            this.btnCleanPass.Text = "清密码";
            this.btnCleanPass.UseVisualStyleBackColor = true;
            this.btnCleanPass.Click += new System.EventHandler(this.BtnCleanPass_Click);
            // 
            // btnGetUserList
            // 
            this.btnGetUserList.Location = new System.Drawing.Point(3, 3);
            this.btnGetUserList.Name = "btnGetUserList";
            this.btnGetUserList.Size = new System.Drawing.Size(75, 23);
            this.btnGetUserList.TabIndex = 5;
            this.btnGetUserList.Text = "获取用户";
            this.btnGetUserList.UseVisualStyleBackColor = true;
            this.btnGetUserList.Click += new System.EventHandler(this.BtnGetUserList_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(247, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "存货核算方式";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.rgCalculateType1);
            this.panel6.Controls.Add(this.rgCalculateType0);
            this.panel6.Location = new System.Drawing.Point(330, 131);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(174, 22);
            this.panel6.TabIndex = 4;
            // 
            // rgCalculateType1
            // 
            this.rgCalculateType1.AutoSize = true;
            this.rgCalculateType1.Location = new System.Drawing.Point(80, 3);
            this.rgCalculateType1.Name = "rgCalculateType1";
            this.rgCalculateType1.Size = new System.Drawing.Size(71, 16);
            this.rgCalculateType1.TabIndex = 3;
            this.rgCalculateType1.TabStop = true;
            this.rgCalculateType1.Text = "分仓核算";
            this.rgCalculateType1.UseVisualStyleBackColor = true;
            // 
            // rgCalculateType0
            // 
            this.rgCalculateType0.AutoSize = true;
            this.rgCalculateType0.Location = new System.Drawing.Point(3, 3);
            this.rgCalculateType0.Name = "rgCalculateType0";
            this.rgCalculateType0.Size = new System.Drawing.Size(71, 16);
            this.rgCalculateType0.TabIndex = 2;
            this.rgCalculateType0.TabStop = true;
            this.rgCalculateType0.Text = "总仓核算";
            this.rgCalculateType0.UseVisualStyleBackColor = true;
            // 
            // chkBatchManual
            // 
            this.chkBatchManual.AutoSize = true;
            this.chkBatchManual.Location = new System.Drawing.Point(249, 65);
            this.chkBatchManual.Name = "chkBatchManual";
            this.chkBatchManual.Size = new System.Drawing.Size(132, 16);
            this.chkBatchManual.TabIndex = 6;
            this.chkBatchManual.Text = "批号可手工改变编码";
            this.chkBatchManual.UseVisualStyleBackColor = true;
            // 
            // plRight
            // 
            this.plRight.Controls.Add(this.panel1);
            this.plRight.Controls.Add(this.label6);
            this.plRight.Controls.Add(this.label5);
            this.plRight.Controls.Add(this.label4);
            this.plRight.Controls.Add(this.chkJOBPAY_NOTCONTROLMAXROW);
            this.plRight.Controls.Add(this.chkICMORptAutoBuildItemScrap);
            this.plRight.Controls.Add(this.chkICMOCONFIRM_RELEASE);
            this.plRight.Controls.Add(this.chkUnderStockCalculate);
            this.plRight.Controls.Add(this.chkUnderStock);
            this.plRight.Controls.Add(this.chkSameUserApprovalAndUnApproval);
            this.plRight.Controls.Add(this.chkRefreshAfterAdd);
            this.plRight.Controls.Add(this.chkPOrderTaxInPrice);
            this.plRight.Controls.Add(this.chkSEOrderCanAppenItem);
            this.plRight.Controls.Add(this.chkPOOrderCanAppenItem);
            this.plRight.Controls.Add(this.chkPoInvSynInPrice);
            this.plRight.Controls.Add(this.chkNoHookSIWhenDiffQty);
            this.plRight.Controls.Add(this.chkNoHookPIWhenDiffQty);
            this.plRight.Controls.Add(this.panel8);
            this.plRight.Controls.Add(this.label3);
            this.plRight.Controls.Add(this.panel7);
            this.plRight.Controls.Add(this.chkCQtyLargeSEQty);
            this.plRight.Controls.Add(this.chkCQtyLargePOQty);
            this.plRight.Controls.Add(this.chkBatchRepeatEcho);
            this.plRight.Controls.Add(this.chkBatchManual);
            this.plRight.Controls.Add(this.panel6);
            this.plRight.Controls.Add(this.label2);
            this.plRight.Controls.Add(this.label1);
            this.plRight.Controls.Add(this.grpUsers);
            this.plRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plRight.Location = new System.Drawing.Point(258, 40);
            this.plRight.Name = "plRight";
            this.plRight.Size = new System.Drawing.Size(541, 564);
            this.plRight.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(249, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 19);
            this.label5.TabIndex = 27;
            this.label5.Text = "供应链参数";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(249, 475);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 19);
            this.label4.TabIndex = 26;
            this.label4.Text = "生产参数";
            // 
            // chkJOBPAY_NOTCONTROLMAXROW
            // 
            this.chkJOBPAY_NOTCONTROLMAXROW.AutoSize = true;
            this.chkJOBPAY_NOTCONTROLMAXROW.Location = new System.Drawing.Point(249, 544);
            this.chkJOBPAY_NOTCONTROLMAXROW.Name = "chkJOBPAY_NOTCONTROLMAXROW";
            this.chkJOBPAY_NOTCONTROLMAXROW.Size = new System.Drawing.Size(180, 16);
            this.chkJOBPAY_NOTCONTROLMAXROW.TabIndex = 25;
            this.chkJOBPAY_NOTCONTROLMAXROW.Text = "计件工资清单放到1000行限制";
            this.chkJOBPAY_NOTCONTROLMAXROW.UseVisualStyleBackColor = true;
            // 
            // chkICMORptAutoBuildItemScrap
            // 
            this.chkICMORptAutoBuildItemScrap.AutoSize = true;
            this.chkICMORptAutoBuildItemScrap.Location = new System.Drawing.Point(249, 522);
            this.chkICMORptAutoBuildItemScrap.Name = "chkICMORptAutoBuildItemScrap";
            this.chkICMORptAutoBuildItemScrap.Size = new System.Drawing.Size(240, 16);
            this.chkICMORptAutoBuildItemScrap.TabIndex = 24;
            this.chkICMORptAutoBuildItemScrap.Text = "任务汇报审核时自动生成生产物料报废单";
            this.chkICMORptAutoBuildItemScrap.UseVisualStyleBackColor = true;
            // 
            // chkICMOCONFIRM_RELEASE
            // 
            this.chkICMOCONFIRM_RELEASE.AutoSize = true;
            this.chkICMOCONFIRM_RELEASE.Location = new System.Drawing.Point(249, 500);
            this.chkICMOCONFIRM_RELEASE.Name = "chkICMOCONFIRM_RELEASE";
            this.chkICMOCONFIRM_RELEASE.Size = new System.Drawing.Size(144, 16);
            this.chkICMOCONFIRM_RELEASE.TabIndex = 23;
            this.chkICMOCONFIRM_RELEASE.Text = "任务单确认时自动下达";
            this.chkICMOCONFIRM_RELEASE.UseVisualStyleBackColor = true;
            // 
            // chkUnderStockCalculate
            // 
            this.chkUnderStockCalculate.AutoSize = true;
            this.chkUnderStockCalculate.Location = new System.Drawing.Point(249, 446);
            this.chkUnderStockCalculate.Name = "chkUnderStockCalculate";
            this.chkUnderStockCalculate.Size = new System.Drawing.Size(108, 16);
            this.chkUnderStockCalculate.TabIndex = 22;
            this.chkUnderStockCalculate.Text = "允许负库存结账";
            this.chkUnderStockCalculate.UseVisualStyleBackColor = true;
            // 
            // chkUnderStock
            // 
            this.chkUnderStock.AutoSize = true;
            this.chkUnderStock.Location = new System.Drawing.Point(249, 424);
            this.chkUnderStock.Name = "chkUnderStock";
            this.chkUnderStock.Size = new System.Drawing.Size(108, 16);
            this.chkUnderStock.TabIndex = 21;
            this.chkUnderStock.Text = "允许负库存出库";
            this.chkUnderStock.UseVisualStyleBackColor = true;
            // 
            // chkSameUserApprovalAndUnApproval
            // 
            this.chkSameUserApprovalAndUnApproval.AutoSize = true;
            this.chkSameUserApprovalAndUnApproval.Location = new System.Drawing.Point(249, 402);
            this.chkSameUserApprovalAndUnApproval.Name = "chkSameUserApprovalAndUnApproval";
            this.chkSameUserApprovalAndUnApproval.Size = new System.Drawing.Size(168, 16);
            this.chkSameUserApprovalAndUnApproval.TabIndex = 20;
            this.chkSameUserApprovalAndUnApproval.Text = "反审核人与审核人为同一人";
            this.chkSameUserApprovalAndUnApproval.UseVisualStyleBackColor = true;
            // 
            // chkRefreshAfterAdd
            // 
            this.chkRefreshAfterAdd.AutoSize = true;
            this.chkRefreshAfterAdd.Location = new System.Drawing.Point(249, 380);
            this.chkRefreshAfterAdd.Name = "chkRefreshAfterAdd";
            this.chkRefreshAfterAdd.Size = new System.Drawing.Size(144, 16);
            this.chkRefreshAfterAdd.TabIndex = 19;
            this.chkRefreshAfterAdd.Text = "序时簿新增后立即刷新";
            this.chkRefreshAfterAdd.UseVisualStyleBackColor = true;
            // 
            // chkPOrderTaxInPrice
            // 
            this.chkPOrderTaxInPrice.AutoSize = true;
            this.chkPOrderTaxInPrice.Location = new System.Drawing.Point(249, 358);
            this.chkPOrderTaxInPrice.Name = "chkPOrderTaxInPrice";
            this.chkPOrderTaxInPrice.Size = new System.Drawing.Size(180, 16);
            this.chkPOrderTaxInPrice.TabIndex = 18;
            this.chkPOrderTaxInPrice.Text = "采购订单单价默认为含税单价";
            this.chkPOrderTaxInPrice.UseVisualStyleBackColor = true;
            // 
            // chkSEOrderCanAppenItem
            // 
            this.chkSEOrderCanAppenItem.AutoSize = true;
            this.chkSEOrderCanAppenItem.Location = new System.Drawing.Point(249, 336);
            this.chkSEOrderCanAppenItem.Name = "chkSEOrderCanAppenItem";
            this.chkSEOrderCanAppenItem.Size = new System.Drawing.Size(192, 16);
            this.chkSEOrderCanAppenItem.TabIndex = 17;
            this.chkSEOrderCanAppenItem.Text = "已关闭的销售订单可以增加物料";
            this.chkSEOrderCanAppenItem.UseVisualStyleBackColor = true;
            // 
            // chkPOOrderCanAppenItem
            // 
            this.chkPOOrderCanAppenItem.AutoSize = true;
            this.chkPOOrderCanAppenItem.Location = new System.Drawing.Point(249, 314);
            this.chkPOOrderCanAppenItem.Name = "chkPOOrderCanAppenItem";
            this.chkPOOrderCanAppenItem.Size = new System.Drawing.Size(192, 16);
            this.chkPOOrderCanAppenItem.TabIndex = 16;
            this.chkPOOrderCanAppenItem.Text = "已关闭的采购订单可以追加物料";
            this.chkPOOrderCanAppenItem.UseVisualStyleBackColor = true;
            // 
            // chkPoInvSynInPrice
            // 
            this.chkPoInvSynInPrice.AutoSize = true;
            this.chkPoInvSynInPrice.Location = new System.Drawing.Point(249, 292);
            this.chkPoInvSynInPrice.Name = "chkPoInvSynInPrice";
            this.chkPoInvSynInPrice.Size = new System.Drawing.Size(204, 16);
            this.chkPoInvSynInPrice.TabIndex = 15;
            this.chkPoInvSynInPrice.Text = "采购单价与蓝字采购发票价格同步";
            this.chkPoInvSynInPrice.UseVisualStyleBackColor = true;
            // 
            // chkNoHookSIWhenDiffQty
            // 
            this.chkNoHookSIWhenDiffQty.AutoSize = true;
            this.chkNoHookSIWhenDiffQty.Location = new System.Drawing.Point(249, 270);
            this.chkNoHookSIWhenDiffQty.Name = "chkNoHookSIWhenDiffQty";
            this.chkNoHookSIWhenDiffQty.Size = new System.Drawing.Size(240, 16);
            this.chkNoHookSIWhenDiffQty.TabIndex = 14;
            this.chkNoHookSIWhenDiffQty.Text = "销售发票和出库单数量不一致不允许勾稽";
            this.chkNoHookSIWhenDiffQty.UseVisualStyleBackColor = true;
            // 
            // chkNoHookPIWhenDiffQty
            // 
            this.chkNoHookPIWhenDiffQty.AutoSize = true;
            this.chkNoHookPIWhenDiffQty.Location = new System.Drawing.Point(249, 248);
            this.chkNoHookPIWhenDiffQty.Name = "chkNoHookPIWhenDiffQty";
            this.chkNoHookPIWhenDiffQty.Size = new System.Drawing.Size(240, 16);
            this.chkNoHookPIWhenDiffQty.TabIndex = 13;
            this.chkNoHookPIWhenDiffQty.Text = "采购发票和入库单数量不一致不允许勾稽";
            this.chkNoHookPIWhenDiffQty.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.rgItemCountPrice1);
            this.panel8.Controls.Add(this.rgItemCountPrice0);
            this.panel8.Location = new System.Drawing.Point(330, 187);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(174, 22);
            this.panel8.TabIndex = 12;
            // 
            // rgItemCountPrice1
            // 
            this.rgItemCountPrice1.AutoSize = true;
            this.rgItemCountPrice1.Location = new System.Drawing.Point(92, 3);
            this.rgItemCountPrice1.Name = "rgItemCountPrice1";
            this.rgItemCountPrice1.Size = new System.Drawing.Size(83, 16);
            this.rgItemCountPrice1.TabIndex = 3;
            this.rgItemCountPrice1.TabStop = true;
            this.rgItemCountPrice1.Text = "计划成本法";
            this.rgItemCountPrice1.UseVisualStyleBackColor = true;
            // 
            // rgItemCountPrice0
            // 
            this.rgItemCountPrice0.AutoSize = true;
            this.rgItemCountPrice0.Location = new System.Drawing.Point(3, 3);
            this.rgItemCountPrice0.Name = "rgItemCountPrice0";
            this.rgItemCountPrice0.Size = new System.Drawing.Size(83, 16);
            this.rgItemCountPrice0.TabIndex = 2;
            this.rgItemCountPrice0.TabStop = true;
            this.rgItemCountPrice0.Text = "实际成本法";
            this.rgItemCountPrice0.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(247, 193);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 11;
            this.label3.Text = "存货计价方法";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.rgInInvProcess1);
            this.panel7.Controls.Add(this.rgInInvProcess0);
            this.panel7.Location = new System.Drawing.Point(330, 159);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(174, 22);
            this.panel7.TabIndex = 10;
            // 
            // rgInInvProcess1
            // 
            this.rgInInvProcess1.AutoSize = true;
            this.rgInInvProcess1.Location = new System.Drawing.Point(92, 3);
            this.rgInInvProcess1.Name = "rgInInvProcess1";
            this.rgInInvProcess1.Size = new System.Drawing.Size(71, 16);
            this.rgInInvProcess1.TabIndex = 3;
            this.rgInInvProcess1.TabStop = true;
            this.rgInInvProcess1.Text = "单到冲回";
            this.rgInInvProcess1.UseVisualStyleBackColor = true;
            // 
            // rgInInvProcess0
            // 
            this.rgInInvProcess0.AutoSize = true;
            this.rgInInvProcess0.Location = new System.Drawing.Point(3, 3);
            this.rgInInvProcess0.Name = "rgInInvProcess0";
            this.rgInInvProcess0.Size = new System.Drawing.Size(83, 16);
            this.rgInInvProcess0.TabIndex = 2;
            this.rgInInvProcess0.TabStop = true;
            this.rgInInvProcess0.Text = "下月初冲回";
            this.rgInInvProcess0.UseVisualStyleBackColor = true;
            // 
            // chkCQtyLargeSEQty
            // 
            this.chkCQtyLargeSEQty.AutoSize = true;
            this.chkCQtyLargeSEQty.Location = new System.Drawing.Point(249, 109);
            this.chkCQtyLargeSEQty.Name = "chkCQtyLargeSEQty";
            this.chkCQtyLargeSEQty.Size = new System.Drawing.Size(216, 16);
            this.chkCQtyLargeSEQty.TabIndex = 9;
            this.chkCQtyLargeSEQty.Text = "销售订单执行数量允许超过订单数量";
            this.chkCQtyLargeSEQty.UseVisualStyleBackColor = true;
            // 
            // chkCQtyLargePOQty
            // 
            this.chkCQtyLargePOQty.AutoSize = true;
            this.chkCQtyLargePOQty.Location = new System.Drawing.Point(249, 87);
            this.chkCQtyLargePOQty.Name = "chkCQtyLargePOQty";
            this.chkCQtyLargePOQty.Size = new System.Drawing.Size(216, 16);
            this.chkCQtyLargePOQty.TabIndex = 8;
            this.chkCQtyLargePOQty.Text = "采购订单执行数量允许超过订单数量";
            this.chkCQtyLargePOQty.UseVisualStyleBackColor = true;
            // 
            // chkBatchRepeatEcho
            // 
            this.chkBatchRepeatEcho.AutoSize = true;
            this.chkBatchRepeatEcho.Location = new System.Drawing.Point(387, 65);
            this.chkBatchRepeatEcho.Name = "chkBatchRepeatEcho";
            this.chkBatchRepeatEcho.Size = new System.Drawing.Size(120, 16);
            this.chkBatchRepeatEcho.TabIndex = 7;
            this.chkBatchRepeatEcho.Text = "批号重复给出提示";
            this.chkBatchRepeatEcho.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(247, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "暂估入库处理";
            // 
            // fUserIDDataGridViewTextBoxColumn
            // 
            this.fUserIDDataGridViewTextBoxColumn.DataPropertyName = "FUserID";
            this.fUserIDDataGridViewTextBoxColumn.HeaderText = "用户ID";
            this.fUserIDDataGridViewTextBoxColumn.Name = "fUserIDDataGridViewTextBoxColumn";
            this.fUserIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fNameDataGridViewTextBoxColumn
            // 
            this.fNameDataGridViewTextBoxColumn.DataPropertyName = "FName";
            this.fNameDataGridViewTextBoxColumn.HeaderText = "用户名";
            this.fNameDataGridViewTextBoxColumn.Name = "fNameDataGridViewTextBoxColumn";
            this.fNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fDescriptionDataGridViewTextBoxColumn
            // 
            this.fDescriptionDataGridViewTextBoxColumn.DataPropertyName = "FDescription";
            this.fDescriptionDataGridViewTextBoxColumn.HeaderText = "说明";
            this.fDescriptionDataGridViewTextBoxColumn.Name = "fDescriptionDataGridViewTextBoxColumn";
            this.fDescriptionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bsUserList
            // 
            this.bsUserList.DataSource = typeof(KDAcctAttach.Models.KDUserModel);
            // 
            // dBNameDataGridViewTextBoxColumn
            // 
            this.dBNameDataGridViewTextBoxColumn.DataPropertyName = "DBName";
            this.dBNameDataGridViewTextBoxColumn.HeaderText = "账套号";
            this.dBNameDataGridViewTextBoxColumn.Name = "dBNameDataGridViewTextBoxColumn";
            this.dBNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dBPathDataGridViewTextBoxColumn
            // 
            this.dBPathDataGridViewTextBoxColumn.DataPropertyName = "DBPath";
            this.dBPathDataGridViewTextBoxColumn.HeaderText = "名称";
            this.dBPathDataGridViewTextBoxColumn.Name = "dBPathDataGridViewTextBoxColumn";
            this.dBPathDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dBVersionDataGridViewTextBoxColumn
            // 
            this.dBVersionDataGridViewTextBoxColumn.DataPropertyName = "DBVersion";
            this.dBVersionDataGridViewTextBoxColumn.HeaderText = "版本号";
            this.dBVersionDataGridViewTextBoxColumn.Name = "dBVersionDataGridViewTextBoxColumn";
            this.dBVersionDataGridViewTextBoxColumn.ReadOnly = true;
            this.dBVersionDataGridViewTextBoxColumn.Width = 70;
            // 
            // bsDBList
            // 
            this.bsDBList.DataSource = typeof(KDAcctAttach.DBItemModel);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rgUPSTOCKWHENSAVE1);
            this.panel1.Controls.Add(this.rgUPSTOCKWHENSAVE0);
            this.panel1.Location = new System.Drawing.Point(330, 215);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(174, 22);
            this.panel1.TabIndex = 29;
            // 
            // rgUPSTOCKWHENSAVE1
            // 
            this.rgUPSTOCKWHENSAVE1.AutoSize = true;
            this.rgUPSTOCKWHENSAVE1.Location = new System.Drawing.Point(92, 3);
            this.rgUPSTOCKWHENSAVE1.Name = "rgUPSTOCKWHENSAVE1";
            this.rgUPSTOCKWHENSAVE1.Size = new System.Drawing.Size(71, 16);
            this.rgUPSTOCKWHENSAVE1.TabIndex = 3;
            this.rgUPSTOCKWHENSAVE1.TabStop = true;
            this.rgUPSTOCKWHENSAVE1.Text = "保存更新";
            this.rgUPSTOCKWHENSAVE1.UseVisualStyleBackColor = true;
            // 
            // rgUPSTOCKWHENSAVE0
            // 
            this.rgUPSTOCKWHENSAVE0.AutoSize = true;
            this.rgUPSTOCKWHENSAVE0.Location = new System.Drawing.Point(3, 3);
            this.rgUPSTOCKWHENSAVE0.Name = "rgUPSTOCKWHENSAVE0";
            this.rgUPSTOCKWHENSAVE0.Size = new System.Drawing.Size(83, 16);
            this.rgUPSTOCKWHENSAVE0.TabIndex = 2;
            this.rgUPSTOCKWHENSAVE0.TabStop = true;
            this.rgUPSTOCKWHENSAVE0.Text = "审核后更新";
            this.rgUPSTOCKWHENSAVE0.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(247, 218);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 28;
            this.label6.Text = "库存更新控制";
            // 
            // FrmTools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 604);
            this.Controls.Add(this.plRight);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.plLeft);
            this.Name = "FrmTools";
            this.Text = "FrmTools";
            this.Shown += new System.EventHandler(this.FrmTools_Shown);
            this.plLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcctList)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.grpUsers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserList)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.plRight.ResumeLayout(false);
            this.plRight.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsUserList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDBList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel plLeft;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgvAcctList;
        private System.Windows.Forms.Button btnRefreshDB;
        private System.Windows.Forms.RadioButton rgAllDB;
        private System.Windows.Forms.RadioButton rgKDAcct;
        private System.Windows.Forms.Button btnGetAcctConfig;
        private System.Windows.Forms.Button btnSaveAcctConfig;
        private System.Windows.Forms.GroupBox grpUsers;
        private System.Windows.Forms.DataGridView dgvUserList;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnCleanProfile;
        private System.Windows.Forms.Button btnCleanPass;
        private System.Windows.Forms.Button btnGetUserList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RadioButton rgCalculateType1;
        private System.Windows.Forms.RadioButton rgCalculateType0;
        private System.Windows.Forms.CheckBox chkBatchManual;
        private System.Windows.Forms.Panel plRight;
        private System.Windows.Forms.CheckBox chkICMOCONFIRM_RELEASE;
        private System.Windows.Forms.CheckBox chkUnderStockCalculate;
        private System.Windows.Forms.CheckBox chkUnderStock;
        private System.Windows.Forms.CheckBox chkSameUserApprovalAndUnApproval;
        private System.Windows.Forms.CheckBox chkRefreshAfterAdd;
        private System.Windows.Forms.CheckBox chkPOrderTaxInPrice;
        private System.Windows.Forms.CheckBox chkSEOrderCanAppenItem;
        private System.Windows.Forms.CheckBox chkPOOrderCanAppenItem;
        private System.Windows.Forms.CheckBox chkPoInvSynInPrice;
        private System.Windows.Forms.CheckBox chkNoHookSIWhenDiffQty;
        private System.Windows.Forms.CheckBox chkNoHookPIWhenDiffQty;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.RadioButton rgItemCountPrice1;
        private System.Windows.Forms.RadioButton rgItemCountPrice0;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.RadioButton rgInInvProcess1;
        private System.Windows.Forms.RadioButton rgInInvProcess0;
        private System.Windows.Forms.CheckBox chkCQtyLargeSEQty;
        private System.Windows.Forms.CheckBox chkCQtyLargePOQty;
        private System.Windows.Forms.CheckBox chkBatchRepeatEcho;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkJOBPAY_NOTCONTROLMAXROW;
        private System.Windows.Forms.CheckBox chkICMORptAutoBuildItemScrap;
        private System.Windows.Forms.BindingSource bsDBList;
        private System.Windows.Forms.DataGridViewTextBoxColumn fUserIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource bsUserList;
        private System.Windows.Forms.DataGridViewTextBoxColumn dBNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dBPathDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dBVersionDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rgUPSTOCKWHENSAVE1;
        private System.Windows.Forms.RadioButton rgUPSTOCKWHENSAVE0;
        private System.Windows.Forms.Label label6;
    }
}