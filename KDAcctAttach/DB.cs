﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SqlSugar;
namespace KDAcctAttach {
    public class DB {
        public DB()
        {
            string connStr = "Server=.;Database=master;Trusted_Connection=true;";
            if (!Comm.DBCfg.LocalDB)
                connStr = $"Server={Comm.DBCfg.Server};Database={Comm.DBCfg.DBName};User Id={Comm.DBCfg.UserName};Password={Comm.DBCfg.Password};Trusted_Connection=false;";
            _db = new SqlSugarClient(new ConnectionConfig
            {
                DbType = DbType.SqlServer,
                ConnectionString = connStr,
                ConfigId = "master",
                //ConnectionString = "Server=192.168.1.238;User Id=sa;Password=fJAq+70g;Trusted_Connection=false;",
                IsAutoCloseConnection = true
            });
            connStr = "Server=.;Database=AcctCtl;Trusted_Connection=true;";
            if (!Comm.DBCfg.LocalDB)
                connStr = $"Server={Comm.DBCfg.Server};Database=AcctCtl;User Id={Comm.DBCfg.UserName};Password={Comm.DBCfg.Password};Trusted_Connection=false;";

            _db.AddConnection(new ConnectionConfig
            {
                DbType = DbType.SqlServer,
                IsAutoCloseConnection = true,
                ConfigId = "AcctCtl",
                ConnectionString = connStr
            });
        }
        public static void GetConn(SqlSugarClient db, string dbName)
        {
            if(!db.IsAnyConnection(dbName))
            {
                string connStr = "Server=.;Database={dbName};Trusted_Connection=true;";
                if (!Comm.DBCfg.LocalDB)
                    connStr = $"Server={Comm.DBCfg.Server};Database={dbName};User Id={Comm.DBCfg.UserName};Password={Comm.DBCfg.Password};Trusted_Connection=false;";
                db.AddConnection(new ConnectionConfig
                {
                    DbType = DbType.SqlServer,
                    IsAutoCloseConnection = true,
                    ConfigId = dbName,
                    ConnectionString = connStr
                });
                db.ChangeDatabase(dbName);
            }
            
        }
        private SqlSugarClient _db;
        public static SqlSugarClient Inst()
        {
            return new DB()._db;
        }
    }
}
