﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KDAcctAttach {
    public static class Comm {
        public static DBConfigModel DBCfg { get; set; }
        public static string AcctCtlName { get; set; } = "AcctCtl";
        public static string ConfigPath { get; set; } = Application.StartupPath + @"\config.set";
        #region 更新服务器地址
        public static string GetUpdServer()
        {
            StreamReader sr = new StreamReader(Path.Combine(Application.StartupPath, "upd.txt"));
            string url = sr.ReadToEnd();
            return url;
        }
        #endregion
        #region 读取配置
        public static void GetConfig()
        {
            try
            {
                    DBCfg = new DBConfigModel { LocalDB = true };
                if(!File.Exists(ConfigPath))
                {
                    return;
                }
                StreamReader sr = new StreamReader(ConfigPath, Encoding.UTF8);
                string jsonStr = sr.ReadToEnd();
                sr.Close();
                sr.Dispose();
                if (string.IsNullOrEmpty(jsonStr))
                    return;
                DBCfg = JsonConvert.DeserializeObject<DBConfigModel>(jsonStr);
            }
            catch (Exception)
            {
            }
        }
        #endregion
        #region 保存配置
        public static void SaveConfig()
        {
            string jsonStr = JsonConvert.SerializeObject(DBCfg);
            StreamWriter sw = new StreamWriter(ConfigPath, false, Encoding.UTF8);
            sw.Write(jsonStr);
            sw.Close();
            sw.Dispose();
        }
        #endregion
        #region 转换是否
        public static bool ToBool(object val)
        {
            if (val is null)
                return false;
            if (val.ToString() == "0")
                return false;
            else
                return true;
        }
        public static int ToInt(bool val)
        {
            if (val)
                return 1;
            else
                return 0;
        }
        #endregion
    }
}
