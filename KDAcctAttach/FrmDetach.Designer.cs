﻿namespace KDAcctAttach {
    partial class FrmDetach {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.plQuery = new System.Windows.Forms.Panel();
            this.lbInfo = new System.Windows.Forms.Label();
            this.btnSaveFiles = new System.Windows.Forms.Button();
            this.btnDetach = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.dgvAcctList = new System.Windows.Forms.DataGridView();
            this.colCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dBNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dBIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dBPathDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dBVersionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cMPTLevelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsDBList = new System.Windows.Forms.BindingSource(this.components);
            this.dgvFiles = new System.Windows.Forms.DataGridView();
            this.fileIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filePathDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileSizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsFiles = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnReverse = new System.Windows.Forms.Button();
            this.btnCancelAll = new System.Windows.Forms.Button();
            this.plQuery.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcctList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDBList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsFiles)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // plQuery
            // 
            this.plQuery.Controls.Add(this.lbInfo);
            this.plQuery.Controls.Add(this.btnSaveFiles);
            this.plQuery.Controls.Add(this.btnDetach);
            this.plQuery.Controls.Add(this.btnRefresh);
            this.plQuery.Dock = System.Windows.Forms.DockStyle.Top;
            this.plQuery.Location = new System.Drawing.Point(0, 0);
            this.plQuery.Name = "plQuery";
            this.plQuery.Size = new System.Drawing.Size(783, 44);
            this.plQuery.TabIndex = 1;
            // 
            // lbInfo
            // 
            this.lbInfo.AutoSize = true;
            this.lbInfo.Location = new System.Drawing.Point(269, 19);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(0, 12);
            this.lbInfo.TabIndex = 3;
            // 
            // btnSaveFiles
            // 
            this.btnSaveFiles.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveFiles.Location = new System.Drawing.Point(174, 12);
            this.btnSaveFiles.Name = "btnSaveFiles";
            this.btnSaveFiles.Size = new System.Drawing.Size(75, 23);
            this.btnSaveFiles.TabIndex = 2;
            this.btnSaveFiles.Text = "复制文件到";
            this.btnSaveFiles.UseVisualStyleBackColor = true;
            this.btnSaveFiles.Click += new System.EventHandler(this.BtnSaveFiles_Click);
            // 
            // btnDetach
            // 
            this.btnDetach.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDetach.Location = new System.Drawing.Point(93, 12);
            this.btnDetach.Name = "btnDetach";
            this.btnDetach.Size = new System.Drawing.Size(75, 23);
            this.btnDetach.TabIndex = 1;
            this.btnDetach.Text = "分离数据库";
            this.btnDetach.UseVisualStyleBackColor = true;
            this.btnDetach.Click += new System.EventHandler(this.BtnDetach_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Location = new System.Drawing.Point(12, 12);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "刷新列表";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.BtnRefresh_Click);
            // 
            // dgvAcctList
            // 
            this.dgvAcctList.AllowUserToAddRows = false;
            this.dgvAcctList.AllowUserToDeleteRows = false;
            this.dgvAcctList.AllowUserToResizeRows = false;
            this.dgvAcctList.AutoGenerateColumns = false;
            this.dgvAcctList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAcctList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCheck,
            this.dBNameDataGridViewTextBoxColumn,
            this.dBIdDataGridViewTextBoxColumn,
            this.dBPathDataGridViewTextBoxColumn,
            this.dBVersionDataGridViewTextBoxColumn,
            this.cMPTLevelDataGridViewTextBoxColumn});
            this.dgvAcctList.DataSource = this.bsDBList;
            this.dgvAcctList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAcctList.Location = new System.Drawing.Point(0, 30);
            this.dgvAcctList.Name = "dgvAcctList";
            this.dgvAcctList.RowHeadersVisible = false;
            this.dgvAcctList.RowTemplate.Height = 23;
            this.dgvAcctList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAcctList.Size = new System.Drawing.Size(783, 347);
            this.dgvAcctList.TabIndex = 2;
            this.dgvAcctList.SelectionChanged += new System.EventHandler(this.DgvAcctList_SelectionChanged);
            // 
            // colCheck
            // 
            this.colCheck.DataPropertyName = "Checked";
            this.colCheck.FalseValue = "false";
            this.colCheck.HeaderText = "选择";
            this.colCheck.Name = "colCheck";
            this.colCheck.TrueValue = "true";
            this.colCheck.Width = 60;
            // 
            // dBNameDataGridViewTextBoxColumn
            // 
            this.dBNameDataGridViewTextBoxColumn.DataPropertyName = "DBName";
            this.dBNameDataGridViewTextBoxColumn.HeaderText = "数据库名";
            this.dBNameDataGridViewTextBoxColumn.Name = "dBNameDataGridViewTextBoxColumn";
            this.dBNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dBIdDataGridViewTextBoxColumn
            // 
            this.dBIdDataGridViewTextBoxColumn.DataPropertyName = "DBId";
            this.dBIdDataGridViewTextBoxColumn.HeaderText = "数据库ID";
            this.dBIdDataGridViewTextBoxColumn.Name = "dBIdDataGridViewTextBoxColumn";
            this.dBIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dBPathDataGridViewTextBoxColumn
            // 
            this.dBPathDataGridViewTextBoxColumn.DataPropertyName = "DBPath";
            this.dBPathDataGridViewTextBoxColumn.HeaderText = "主路径";
            this.dBPathDataGridViewTextBoxColumn.Name = "dBPathDataGridViewTextBoxColumn";
            this.dBPathDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dBVersionDataGridViewTextBoxColumn
            // 
            this.dBVersionDataGridViewTextBoxColumn.DataPropertyName = "DBVersion";
            this.dBVersionDataGridViewTextBoxColumn.HeaderText = "版本";
            this.dBVersionDataGridViewTextBoxColumn.Name = "dBVersionDataGridViewTextBoxColumn";
            this.dBVersionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cMPTLevelDataGridViewTextBoxColumn
            // 
            this.cMPTLevelDataGridViewTextBoxColumn.DataPropertyName = "CMPTLevel";
            this.cMPTLevelDataGridViewTextBoxColumn.HeaderText = "兼容级别";
            this.cMPTLevelDataGridViewTextBoxColumn.Name = "cMPTLevelDataGridViewTextBoxColumn";
            this.cMPTLevelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bsDBList
            // 
            this.bsDBList.DataSource = typeof(KDAcctAttach.DBItemModel);
            // 
            // dgvFiles
            // 
            this.dgvFiles.AllowUserToAddRows = false;
            this.dgvFiles.AllowUserToDeleteRows = false;
            this.dgvFiles.AllowUserToResizeRows = false;
            this.dgvFiles.AutoGenerateColumns = false;
            this.dgvFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fileIdDataGridViewTextBoxColumn,
            this.fileNameDataGridViewTextBoxColumn,
            this.filePathDataGridViewTextBoxColumn,
            this.fileSizeDataGridViewTextBoxColumn});
            this.dgvFiles.DataSource = this.bsFiles;
            this.dgvFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFiles.Location = new System.Drawing.Point(0, 0);
            this.dgvFiles.Name = "dgvFiles";
            this.dgvFiles.ReadOnly = true;
            this.dgvFiles.RowHeadersVisible = false;
            this.dgvFiles.RowTemplate.Height = 23;
            this.dgvFiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFiles.Size = new System.Drawing.Size(783, 100);
            this.dgvFiles.TabIndex = 3;
            // 
            // fileIdDataGridViewTextBoxColumn
            // 
            this.fileIdDataGridViewTextBoxColumn.DataPropertyName = "FileId";
            this.fileIdDataGridViewTextBoxColumn.HeaderText = "文件序号";
            this.fileIdDataGridViewTextBoxColumn.Name = "fileIdDataGridViewTextBoxColumn";
            this.fileIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fileNameDataGridViewTextBoxColumn
            // 
            this.fileNameDataGridViewTextBoxColumn.DataPropertyName = "FileName";
            this.fileNameDataGridViewTextBoxColumn.HeaderText = "文件名";
            this.fileNameDataGridViewTextBoxColumn.Name = "fileNameDataGridViewTextBoxColumn";
            this.fileNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // filePathDataGridViewTextBoxColumn
            // 
            this.filePathDataGridViewTextBoxColumn.DataPropertyName = "FilePath";
            this.filePathDataGridViewTextBoxColumn.HeaderText = "路径";
            this.filePathDataGridViewTextBoxColumn.Name = "filePathDataGridViewTextBoxColumn";
            this.filePathDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fileSizeDataGridViewTextBoxColumn
            // 
            this.fileSizeDataGridViewTextBoxColumn.DataPropertyName = "FileSize";
            this.fileSizeDataGridViewTextBoxColumn.HeaderText = "大小";
            this.fileSizeDataGridViewTextBoxColumn.Name = "fileSizeDataGridViewTextBoxColumn";
            this.fileSizeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bsFiles
            // 
            this.bsFiles.DataSource = typeof(KDAcctAttach.DBFilesModel);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvAcctList);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(783, 377);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvFiles);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 421);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(783, 100);
            this.panel2.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnCancelAll);
            this.panel3.Controls.Add(this.btnReverse);
            this.panel3.Controls.Add(this.btnSelectAll);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(783, 30);
            this.panel3.TabIndex = 3;
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelectAll.Location = new System.Drawing.Point(12, 4);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(44, 23);
            this.btnSelectAll.TabIndex = 1;
            this.btnSelectAll.Text = "全选";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.BtnSelectAll_Click);
            // 
            // btnReverse
            // 
            this.btnReverse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReverse.Location = new System.Drawing.Point(62, 4);
            this.btnReverse.Name = "btnReverse";
            this.btnReverse.Size = new System.Drawing.Size(44, 23);
            this.btnReverse.TabIndex = 2;
            this.btnReverse.Text = "反选";
            this.btnReverse.UseVisualStyleBackColor = true;
            this.btnReverse.Click += new System.EventHandler(this.BtnReverse_Click);
            // 
            // btnCancelAll
            // 
            this.btnCancelAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelAll.Location = new System.Drawing.Point(112, 4);
            this.btnCancelAll.Name = "btnCancelAll";
            this.btnCancelAll.Size = new System.Drawing.Size(44, 23);
            this.btnCancelAll.TabIndex = 3;
            this.btnCancelAll.Text = "全消";
            this.btnCancelAll.UseVisualStyleBackColor = true;
            this.btnCancelAll.Click += new System.EventHandler(this.BtnCancelAll_Click);
            // 
            // FrmDetach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 521);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.plQuery);
            this.Name = "FrmDetach";
            this.Text = "分离数据库";
            this.Shown += new System.EventHandler(this.FrmDetach_Shown);
            this.Resize += new System.EventHandler(this.FrmDetach_Resize);
            this.plQuery.ResumeLayout(false);
            this.plQuery.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcctList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDBList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsFiles)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel plQuery;
        private System.Windows.Forms.Button btnDetach;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridView dgvAcctList;
        private System.Windows.Forms.BindingSource bsDBList;
        private System.Windows.Forms.DataGridView dgvFiles;
        private System.Windows.Forms.BindingSource bsFiles;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn filePathDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn dBNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dBIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dBPathDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dBVersionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cMPTLevelDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnSaveFiles;
        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnCancelAll;
        private System.Windows.Forms.Button btnReverse;
        private System.Windows.Forms.Button btnSelectAll;
    }
}