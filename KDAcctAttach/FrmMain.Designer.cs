﻿namespace KDAcctAttach {
    partial class FrmMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tbSetting = new System.Windows.Forms.TabPage();
            this.tbAttach = new System.Windows.Forms.TabPage();
            this.tbDetach = new System.Windows.Forms.TabPage();
            this.tbTools = new System.Windows.Forms.TabPage();
            this.tbQuery = new System.Windows.Forms.TabPage();
            this.tabMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tbSetting);
            this.tabMain.Controls.Add(this.tbAttach);
            this.tabMain.Controls.Add(this.tbDetach);
            this.tabMain.Controls.Add(this.tbTools);
            this.tabMain.Controls.Add(this.tbQuery);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(1089, 708);
            this.tabMain.TabIndex = 0;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.TabMain_SelectedIndexChanged);
            // 
            // tbSetting
            // 
            this.tbSetting.Location = new System.Drawing.Point(4, 22);
            this.tbSetting.Name = "tbSetting";
            this.tbSetting.Size = new System.Drawing.Size(1081, 682);
            this.tbSetting.TabIndex = 0;
            this.tbSetting.Text = "连接设置";
            this.tbSetting.UseVisualStyleBackColor = true;
            // 
            // tbAttach
            // 
            this.tbAttach.Location = new System.Drawing.Point(4, 22);
            this.tbAttach.Name = "tbAttach";
            this.tbAttach.Size = new System.Drawing.Size(1081, 682);
            this.tbAttach.TabIndex = 1;
            this.tbAttach.Text = "数据库附加";
            this.tbAttach.UseVisualStyleBackColor = true;
            // 
            // tbDetach
            // 
            this.tbDetach.Location = new System.Drawing.Point(4, 22);
            this.tbDetach.Name = "tbDetach";
            this.tbDetach.Size = new System.Drawing.Size(1081, 682);
            this.tbDetach.TabIndex = 2;
            this.tbDetach.Text = "数据库分离";
            this.tbDetach.UseVisualStyleBackColor = true;
            // 
            // tbTools
            // 
            this.tbTools.Location = new System.Drawing.Point(4, 22);
            this.tbTools.Name = "tbTools";
            this.tbTools.Size = new System.Drawing.Size(1081, 682);
            this.tbTools.TabIndex = 3;
            this.tbTools.Text = "金蝶工具";
            this.tbTools.UseVisualStyleBackColor = true;
            // 
            // tbQuery
            // 
            this.tbQuery.Location = new System.Drawing.Point(4, 22);
            this.tbQuery.Name = "tbQuery";
            this.tbQuery.Size = new System.Drawing.Size(1081, 682);
            this.tbQuery.TabIndex = 4;
            this.tbQuery.Text = "命令执行";
            this.tbQuery.UseVisualStyleBackColor = true;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 708);
            this.Controls.Add(this.tabMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "数据库工具";
            this.Shown += new System.EventHandler(this.FrmMain_Shown);
            this.tabMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tbSetting;
        private System.Windows.Forms.TabPage tbAttach;
        private System.Windows.Forms.TabPage tbDetach;
        private System.Windows.Forms.TabPage tbTools;
        private System.Windows.Forms.TabPage tbQuery;
    }
}