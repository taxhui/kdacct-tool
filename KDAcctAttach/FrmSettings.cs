﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KDAcctAttach {
    public partial class FrmSettings : Form {
        public FrmSettings()
        {
            InitializeComponent();
        }
        #region 启用/禁用界面
        private void EnBtn(bool s)
        {
            tbxServer.Enabled = s;
            tbxUserName.Enabled = s;
            tbxPassword.Enabled = s;
        }
        #endregion
        #region 窗口载入
        private void FrmSettings_Shown(object sender, EventArgs e)
        {
            ReloadConfig();
        }

        private void ReloadConfig()
        {
            Comm.GetConfig();
            tbxServer.Text = Comm.DBCfg.Server;
            tbxUserName.Text = Comm.DBCfg.UserName;
            tbxPassword.Text = Comm.DBCfg.Password;
            if (Comm.DBCfg.LocalDB)
            {
                EnBtn(false);
                rgLocalDB.Checked = true;
                rgRemoteDB.Checked = false;
            }
            else
            {
                EnBtn(true);
                rgLocalDB.Checked = false;
                rgRemoteDB.Checked = true;
            }
        }
        #endregion

        #region 选择认证方式
        private void RgLocalDB_CheckedChanged(object sender, EventArgs e)
        {
            if(rgLocalDB.Checked)
            {
                EnBtn(false);
            }
            else
            {
                EnBtn(true);
            }
        }

        #endregion
        #region 刷新参数 
        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ReloadConfig();
        }
        #endregion
        #region 保存配置
        private void BtnSaveConfig_Click(object sender, EventArgs e)
        {
            try
            {
                Comm.DBCfg.Server = tbxServer.Text.Trim();
                Comm.DBCfg.UserName = tbxUserName.Text.Trim();
                Comm.DBCfg.Password = tbxPassword.Text.Trim();
                Comm.DBCfg.DBName = "master";
                Comm.DBCfg.LocalDB = rgLocalDB.Checked;
                Comm.SaveConfig();
                MessageBox.Show("OK");
            }
            catch (Exception ex)
            {
                Comm.GetConfig();
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
        #region 测试连接 
        private void BtnTestConnection_Click(object sender, EventArgs e)
        {
            try
            {
                Comm.DBCfg.Server = tbxServer.Text.Trim();
                Comm.DBCfg.UserName = tbxUserName.Text.Trim();
                Comm.DBCfg.Password = tbxPassword.Text.Trim();
                Comm.DBCfg.DBName = "master";
                Comm.DBCfg.LocalDB = rgLocalDB.Checked;
                var db = DB.Inst();
                var lst = db.Ado.SqlQuery<dynamic>("select top 1 * from sysdatabases;");
                db.Dispose();
                MessageBox.Show("OK");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
