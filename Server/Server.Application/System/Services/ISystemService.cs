﻿namespace Server.Application {
    public interface ISystemService {
        string GetDescription();
    }
}