﻿using Furion;

using System.Reflection;

namespace Server.Web.Entry {
    public class SingleFilePublish : ISingleFilePublish {
        public Assembly[] IncludeAssemblies()
        {
            return Array.Empty<Assembly>();
        }

        public string[] IncludeAssemblyNames()
        {
            return new[]
            {
            "Server.Application",
            "Server.Core",
            "Server.Web.Core"
        };
        }
    }
}